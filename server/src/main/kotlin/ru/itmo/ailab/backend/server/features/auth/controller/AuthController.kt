package ru.itmo.ailab.backend.server.features.auth.controller

import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import ru.itmo.ailab.backend.server.features.auth.services.AuthService

@Tag(name = "Authorization")
@RestController
@RequestMapping("/api/v1/auth")
class AuthController(
    private val authService: AuthService
) {
    @GetMapping("/me")
    fun me() = authService.currentUser()
}
