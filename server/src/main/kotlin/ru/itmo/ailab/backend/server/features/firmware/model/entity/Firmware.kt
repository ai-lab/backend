package ru.itmo.ailab.backend.server.features.firmware.model.entity

import org.hibernate.annotations.Check
import ru.itmo.ailab.backend.server.features.document.model.entity.Document
import ru.itmo.ailab.backend.server.shared.model.entity.ExternalEntity
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.OneToOne
import javax.validation.Valid
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull

@Entity
@Check(constraints = "trim(name) <> ''")
class Firmware(
    @field:NotBlank
    @field:Column(nullable = false)
    var name: String,

    @field:Valid
    @field:NotNull
    @field:OneToOne(optional = false)
    var file: Document,
) : ExternalEntity()
