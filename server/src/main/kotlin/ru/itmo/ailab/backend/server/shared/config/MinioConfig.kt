package ru.itmo.ailab.backend.server.shared.config

import io.minio.MinioClient
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import org.springframework.transaction.annotation.Transactional

@Configuration
class MinioConfig(
    @Value("\${minio.url}")
    private val url: String,

    @Value("\${minio.access-key}")
    private val accessKey: String,

    @Value("\${minio.secret-key}")
    private val secretKey: String,
) {
    @Bean
    @Primary
    @Transactional
    fun minioClient(): MinioClient =
        MinioClient.Builder()
            .endpoint(url)
            .credentials(accessKey, secretKey)
            .build()
}
