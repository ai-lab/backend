package ru.itmo.ailab.backend.server.features.document.model.entity

import org.hibernate.annotations.Check
import ru.itmo.ailab.backend.server.shared.utils.NotBlankOrNull
import ru.itmo.ailab.backend.server.shared.model.entity.ExternalEntity
import javax.persistence.Column
import javax.persistence.Entity
import javax.validation.constraints.NotBlank

@Entity
@Check(
    constraints = "trim(filename) <> '' " +
            "and (media_type = null or trim(media_type) <> '')"
)
class Document(
    @field:NotBlank
    @field:Column(nullable = false)
    var filename: String,

    @field:NotBlankOrNull
    var mediaType: String?,

    @field:NotBlank
    @field:Column(nullable = false)
    var path: String
) : ExternalEntity()
