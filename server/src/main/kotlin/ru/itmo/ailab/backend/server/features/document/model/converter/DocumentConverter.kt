package ru.itmo.ailab.backend.server.features.document.model.converter

import org.springframework.stereotype.Component
import ru.itmo.ailab.backend.server.features.document.model.dto.FileDto
import ru.itmo.ailab.backend.server.features.document.model.entity.Document
import ru.itmo.ailab.backend.server.shared.model.converter.EntityDtoConverter

@Component
class DocumentConverter : EntityDtoConverter<Document, FileDto> {
    fun toEntityWithPath(dto: FileDto, path: String, filename: String) =
        Document(dto.originalFilename ?: filename, dto.contentType, path)
}
