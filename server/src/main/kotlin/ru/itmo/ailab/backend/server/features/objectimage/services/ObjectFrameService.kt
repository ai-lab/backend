package ru.itmo.ailab.backend.server.features.objectimage.services

import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import ru.itmo.ailab.backend.server.features.auth.model.dto.ROLE_STUDENT
import ru.itmo.ailab.backend.server.features.objectimage.model.converter.ObjectFrameConverter
import ru.itmo.ailab.backend.server.features.objectimage.model.dto.ObjectFrameDto
import ru.itmo.ailab.backend.server.features.objectimage.model.entity.ObjectFrame
import ru.itmo.ailab.backend.server.features.objectimage.model.entity.ObjectImage
import ru.itmo.ailab.backend.server.features.objectimage.model.repository.ObjectFrameRepository

@PreAuthorize("hasRole('$ROLE_STUDENT')")
@Service
class ObjectFrameService(
    private val objectFrameRepository: ObjectFrameRepository,
    private val objectFrameConverter: ObjectFrameConverter
) {
    @Transactional
    fun save(objectFrame: ObjectFrame) =
        objectFrameRepository.save(objectFrame)

    @Transactional
    fun saveWithImage(objectFrameDto: ObjectFrameDto, objectImage: ObjectImage) =
        save(objectFrameConverter.toEntityWithImage(objectFrameDto, objectImage))
}
