package ru.itmo.ailab.backend.server.shared.model.entity

import org.hibernate.annotations.Check
import java.util.UUID
import javax.persistence.Column
import javax.persistence.MappedSuperclass
import javax.validation.constraints.NotNull

typealias ExternalId = UUID

@MappedSuperclass
@Check(constraints = "trim(external_id) <> ''")
abstract class ExternalEntity(
    @field:NotNull
    @field:Column(nullable = false, unique = true)
    var externalId: ExternalId = UUID.randomUUID()
) : BaseEntity()
