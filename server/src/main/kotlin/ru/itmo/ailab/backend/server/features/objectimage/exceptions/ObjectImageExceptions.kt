package ru.itmo.ailab.backend.server.features.objectimage.exceptions

import org.springframework.http.HttpStatus
import org.springframework.web.server.ResponseStatusException

class ObjectImageNotFoundException(message: String = "Object image not found") :
    ResponseStatusException(HttpStatus.NOT_FOUND, message)

class NotObjectImageOwnerException(message: String = "Requester is not the object image owner") :
    ResponseStatusException(HttpStatus.NOT_FOUND, message)

class InvalidContentTypeException(message: String = "Image file type required") :
    ResponseStatusException(HttpStatus.NOT_FOUND, message)
