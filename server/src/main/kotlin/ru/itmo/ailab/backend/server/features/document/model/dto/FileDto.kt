package ru.itmo.ailab.backend.server.features.document.model.dto

import org.springframework.web.multipart.MultipartFile

typealias FileDto = MultipartFile
