package ru.itmo.ailab.backend.server.shared.model.converter

interface EntityDtoConverter<Entity, Dto> {
    fun toDto(entity: Entity): Dto {
        throw NotImplementedError()
    }

    fun toEntity(dto: Dto): Entity {
        throw NotImplementedError()
    }
}
