package ru.itmo.ailab.backend.server.features.objectimage.model.dto

import ru.itmo.ailab.backend.server.shared.model.entity.ExternalId
import javax.validation.Valid
import javax.validation.constraints.NotNull

data class ObjectImageDto(
    @field:NotNull
    val id: ExternalId,

    @field:Valid
    val objectFrame: ObjectFrameDto?
)
