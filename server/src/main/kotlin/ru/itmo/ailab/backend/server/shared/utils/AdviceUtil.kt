package ru.itmo.ailab.backend.server.shared.utils

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import ru.itmo.ailab.backend.server.shared.controller.dto.MessageDto

fun <E : Exception> errorResponse(
    exception: E,
    status: HttpStatus,
    getMessage: (e: E) -> String?
) = ResponseEntity
    .status(status)
    .body(getMessage(exception)?.let { MessageDto(it) })

fun <T : Exception> errorResponse(exception: T, status: HttpStatus) =
    errorResponse(exception, status) { it.message }
