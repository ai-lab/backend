package ru.itmo.ailab.backend.server.features.firmware.model.converter

import org.springframework.stereotype.Component
import ru.itmo.ailab.backend.server.features.firmware.model.dto.FirmwareDto
import ru.itmo.ailab.backend.server.features.firmware.model.entity.Firmware
import ru.itmo.ailab.backend.server.shared.model.converter.EntityDtoConverter

@Component
class FirmwareConverter : EntityDtoConverter<Firmware, FirmwareDto> {
    override fun toDto(entity: Firmware) = FirmwareDto(
        entity.externalId,
        entity.name
    )
}
