package ru.itmo.ailab.backend.server.features.firmware.controller

import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.data.domain.Pageable
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*
import ru.itmo.ailab.backend.server.features.document.model.dto.FileDto
import ru.itmo.ailab.backend.server.features.firmware.services.FirmwareService
import ru.itmo.ailab.backend.server.shared.model.entity.ExternalId
import javax.validation.Valid

@Tag(name = "Firmwares")
@RestController
@RequestMapping("/api/v1/firmwares")
class FirmwareController(
    private val firmwareService: FirmwareService,
) {
    @GetMapping
    fun getFirmwareIds(pageable: Pageable) =
        firmwareService.getFirmwareIds(pageable)

    @GetMapping("/{id}")
    fun getFirmware(@PathVariable id: ExternalId) =
        firmwareService.getFirmware(id)

    @GetMapping("/{id}/download")
    fun downloadFirmware(@PathVariable id: ExternalId) =
        firmwareService.downloadFirmware(id)

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(consumes = [MediaType.MULTIPART_FORM_DATA_VALUE])
    fun addFirmware(
        @RequestPart("firmware") firmware: FileDto,
        @RequestParam(required = false) @Valid name: String?
    ) = firmwareService.addFirmware(firmware, name)

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/{id}")
    fun deleteFirmware(@PathVariable id: ExternalId) =
        firmwareService.deleteFirmware(id)
}
