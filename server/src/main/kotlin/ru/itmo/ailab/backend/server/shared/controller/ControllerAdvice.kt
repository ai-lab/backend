package ru.itmo.ailab.backend.server.shared.controller

import com.fasterxml.jackson.databind.PropertyNamingStrategies
import org.postgresql.util.PSQLException
import org.springframework.http.HttpStatus
import org.springframework.http.converter.HttpMessageNotReadableException
import org.springframework.validation.FieldError
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.RestControllerAdvice
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException
import ru.itmo.ailab.backend.server.shared.utils.errorResponse
import javax.validation.ValidationException

@RestControllerAdvice
class ControllerAdvice {
    @ExceptionHandler(
        ValidationException::class,
        HttpMessageNotReadableException::class,
        MethodArgumentTypeMismatchException::class
    )
    fun handleExceptionWithMessage(e: Exception) =
        errorResponse(e, HttpStatus.BAD_REQUEST)

    @ExceptionHandler(MethodArgumentNotValidException::class)
    fun handleMethodArgumentNotValidException(e: MethodArgumentNotValidException) =
        errorResponse(e, HttpStatus.BAD_REQUEST) {
            e.bindingResult.allErrors.joinToString(", ") {
                val fieldName = PropertyNamingStrategies.SnakeCaseStrategy()
                    .translate((it as FieldError).field)
                "$fieldName ${it.defaultMessage}"
            }
        }

    @ExceptionHandler(PSQLException::class)
    fun handlePSQLException(e: PSQLException) =
        errorResponse(e, HttpStatus.BAD_REQUEST) { it.serverErrorMessage?.detail }
}
