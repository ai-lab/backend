package ru.itmo.ailab.backend.server.features.objectimage.services

import org.springframework.data.domain.Pageable
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import ru.itmo.ailab.backend.server.features.auth.model.dto.ROLE_STUDENT
import ru.itmo.ailab.backend.server.features.document.model.dto.FileDto
import ru.itmo.ailab.backend.server.features.document.services.DocumentService
import ru.itmo.ailab.backend.server.features.objectimage.exceptions.InvalidContentTypeException
import ru.itmo.ailab.backend.server.features.objectimage.exceptions.NotObjectImageOwnerException
import ru.itmo.ailab.backend.server.features.objectimage.exceptions.ObjectImageNotFoundException
import ru.itmo.ailab.backend.server.features.objectimage.model.converter.ObjectImageConverter
import ru.itmo.ailab.backend.server.features.objectimage.model.dto.ObjectFrameDto
import ru.itmo.ailab.backend.server.features.objectimage.model.dto.ObjectImageDto
import ru.itmo.ailab.backend.server.features.objectimage.model.entity.ObjectImage
import ru.itmo.ailab.backend.server.features.objectimage.model.repository.ObjectImageRepository
import ru.itmo.ailab.backend.server.features.student.model.entity.Student
import ru.itmo.ailab.backend.server.features.student.services.StudentService
import ru.itmo.ailab.backend.server.shared.model.converter.PageConverter
import ru.itmo.ailab.backend.server.shared.model.entity.ExternalId

@PreAuthorize("hasRole('$ROLE_STUDENT')")
@Service
class ObjectImageService(
    private val objectImageRepository: ObjectImageRepository,
    private val objectFrameService: ObjectFrameService,
    private val studentService: StudentService,
    private val documentService: DocumentService,
    private val objectImageConverter: ObjectImageConverter,
    private val pageConverter: PageConverter,
) {
    companion object {
        private const val OBJECT_IMAGES_DIRECTORY = "object-images"
    }

    private fun findCurrentStudentImages(pageable: Pageable) = objectImageRepository
        .findAllByStudentId(studentService.currentStudent().id, pageable)

    private fun findObjectImageByExternalId(externalId: ExternalId) =
        objectImageRepository.findByExternalId(externalId)
            .orElseThrow { ObjectImageNotFoundException() }

    private fun findImageAndCheckOwner(
        externalId: ExternalId,
        student: Student = studentService.currentStudent()
    ) = findObjectImageByExternalId(externalId).also {
        if (student.id !== it.student.id) throw NotObjectImageOwnerException()
    }

    fun getCurrentStudentImageIds(pageable: Pageable) =
        pageConverter.toDto(findCurrentStudentImages(pageable).map { it.externalId })

    fun getObjectImage(externalId: ExternalId) =
        objectImageConverter.toDto(findImageAndCheckOwner(externalId))

    fun previewObjectImage(externalId: ExternalId) =
        documentService.preview(findImageAndCheckOwner(externalId).image)

    fun downloadObjectImage(externalId: ExternalId) =
        documentService.download(findImageAndCheckOwner(externalId).image)

    @Transactional
    fun addObjectImage(image: FileDto): ObjectImageDto {
        if (image.contentType?.contains("image") == false) throw InvalidContentTypeException()
        val currentStudent = studentService.currentStudent()
        val document = documentService.uploadForStudent(image, OBJECT_IMAGES_DIRECTORY, currentStudent.id)
        return objectImageConverter.toDto(objectImageRepository.save(ObjectImage(document, currentStudent)))
    }

    @Transactional
    fun updateObjectFrame(externalId: ExternalId, objectFrameDto: ObjectFrameDto): ObjectImageDto {
        val objectImage = findImageAndCheckOwner(externalId)
        objectImage.objectFrame = objectFrameService.saveWithImage(objectFrameDto, objectImage)
        return objectImageConverter.toDto(objectImage)
    }

    @Transactional
    fun deleteImage(externalId: ExternalId) {
        val objectImage = findImageAndCheckOwner(externalId)
        objectImageRepository.delete(objectImage)
        documentService.delete(objectImage.image)
    }
}
