package ru.itmo.ailab.backend.server.features.student.model.entity

import org.hibernate.annotations.Check
import ru.itmo.ailab.backend.server.features.auth.model.dto.UserId
import ru.itmo.ailab.backend.server.features.objectimage.model.entity.ObjectImage
import ru.itmo.ailab.backend.server.shared.model.entity.BaseEntity
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.OneToMany
import javax.validation.Valid
import javax.validation.constraints.NotNull

@Entity
@Check(constraints = "trim(user_id) <> ''")
class Student(
    @field:NotNull
    @field:Column(nullable = false)
    var userId: UserId,

    @field:OneToMany(mappedBy = "student")
    var objectImages: List<@Valid @NotNull ObjectImage>
) : BaseEntity()
