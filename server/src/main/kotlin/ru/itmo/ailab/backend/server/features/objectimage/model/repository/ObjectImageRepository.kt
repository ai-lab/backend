package ru.itmo.ailab.backend.server.features.objectimage.model.repository

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import ru.itmo.ailab.backend.server.features.objectimage.model.entity.ObjectImage
import ru.itmo.ailab.backend.server.shared.model.entity.BaseId
import ru.itmo.ailab.backend.server.shared.model.entity.ExternalId
import java.util.Optional

interface ObjectImageRepository : JpaRepository<ObjectImage, Long> {
    fun findByExternalId(externalId: ExternalId): Optional<ObjectImage>

    fun findAllByStudentId(studentId: BaseId, pageable: Pageable): Page<ObjectImage>
}
