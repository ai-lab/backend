package ru.itmo.ailab.backend.server.features.objectimage.model.dto

import ru.itmo.ailab.backend.server.features.objectimage.utils.ImageUtil
import javax.validation.constraints.Max
import javax.validation.constraints.Min
import javax.validation.constraints.NotNull

class CoordinatesDto(
    @field:Min(0)
    @field:Max(ImageUtil.MAX_IMAGE_WIDTH.toLong())
    @field:NotNull
    var x: Double,

    @field:Min(0)
    @field:Max(ImageUtil.MAX_IMAGE_HEIGHT.toLong())
    @field:NotNull
    var y: Double,
)
