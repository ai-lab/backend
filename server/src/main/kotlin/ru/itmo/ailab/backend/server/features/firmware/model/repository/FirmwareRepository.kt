package ru.itmo.ailab.backend.server.features.firmware.model.repository

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import ru.itmo.ailab.backend.server.features.firmware.model.entity.Firmware
import ru.itmo.ailab.backend.server.shared.model.entity.BaseId
import ru.itmo.ailab.backend.server.shared.model.entity.ExternalId
import java.util.Optional

interface FirmwareRepository : JpaRepository<Firmware, BaseId> {
    @Query("select externalId from Firmware")
    fun findAllExternalIds(pageable: Pageable): Page<ExternalId>

    fun findByExternalId(externalId: ExternalId): Optional<Firmware>
}
