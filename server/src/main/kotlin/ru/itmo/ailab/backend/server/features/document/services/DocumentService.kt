package ru.itmo.ailab.backend.server.features.document.services

import org.springframework.core.io.InputStreamResource
import org.springframework.http.ContentDisposition
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import ru.itmo.ailab.backend.server.features.document.model.converter.DocumentConverter
import ru.itmo.ailab.backend.server.features.document.model.dto.FileDto
import ru.itmo.ailab.backend.server.features.document.model.entity.Document
import ru.itmo.ailab.backend.server.features.document.model.repository.DocumentRepository
import ru.itmo.ailab.backend.server.shared.model.entity.BaseId
import java.nio.charset.StandardCharsets
import java.util.UUID

@Service
class DocumentService(
    private val documentRepository: DocumentRepository,
    private val documentConverter: DocumentConverter,
    private val minioService: MinioService,
) {
    @Transactional
    fun upload(file: FileDto, directory: String): Document {
        val fileId = UUID.randomUUID()
        val path = "$directory/$fileId"
        minioService.upload(file, path)
        val document = documentConverter.toEntityWithPath(file, path, fileId.toString());
        document.externalId = fileId
        return documentRepository.save(document)
    }

    @Transactional
    fun uploadForStudent(file: FileDto, directory: String, studentId: BaseId) =
        upload(file, "students/$studentId/$directory")

    private fun documentToResponse(
        document: Document,
        contentDispositionBuilder: ContentDisposition.Builder
    ): ResponseEntity<InputStreamResource> {
        val responseEntity = ResponseEntity.ok()

        if (document.mediaType != null) {
            runCatching { MediaType.parseMediaType(document.mediaType!!) }
                .onSuccess { responseEntity.contentType(it) }
        }

        val contentDisposition = contentDispositionBuilder
            .filename(document.filename, StandardCharsets.UTF_8)
            .build()
            .toString()

        return responseEntity
            .header(HttpHeaders.CONTENT_DISPOSITION, contentDisposition)
            .body(InputStreamResource(minioService.download(document.path)))
    }

    fun preview(document: Document) = documentToResponse(document, ContentDisposition.inline())

    fun download(document: Document) = documentToResponse(document, ContentDisposition.attachment())

    @Transactional
    fun delete(document: Document) {
        minioService.delete(document.path)
        documentRepository.delete(document)
    }
}
