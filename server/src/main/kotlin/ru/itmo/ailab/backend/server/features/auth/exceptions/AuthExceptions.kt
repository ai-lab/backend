package ru.itmo.ailab.backend.server.features.auth.exceptions

import org.springframework.http.HttpStatus
import org.springframework.web.server.ResponseStatusException

class UnauthorizedException : ResponseStatusException(HttpStatus.UNAUTHORIZED)
