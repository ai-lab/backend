package ru.itmo.ailab.backend.server.features.auth.services

import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.oauth2.jwt.Jwt
import org.springframework.stereotype.Service
import ru.itmo.ailab.backend.server.features.auth.exceptions.UnauthorizedException
import ru.itmo.ailab.backend.server.features.auth.model.dto.UserDto
import ru.itmo.ailab.backend.server.features.auth.utils.JwtUtil

@Service
class AuthService {
    fun currentUser(): UserDto {
        val authentication = SecurityContextHolder.getContext().authentication
            ?: throw UnauthorizedException()
        val jwt = authentication.principal as Jwt
        return JwtUtil.extractUser(jwt)
    }
}
