package ru.itmo.ailab.backend.server.shared.controller.dto

import javax.validation.constraints.NotBlank

data class MessageDto(
    @field:NotBlank
    val message: String
)
