package ru.itmo.ailab.backend.server.shared.config

import io.swagger.v3.oas.models.OpenAPI
import io.swagger.v3.oas.models.info.Info
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class OpenApiConfig {
    @Bean
    fun openApi(@Value("\${springdoc.version}") appVersion: String): OpenAPI =
        OpenAPI().info(
            Info()
                .title("AI lab")
                .version(appVersion)
                .description("AI lab server API")
        )
}
