package ru.itmo.ailab.backend.server.features.auth.utils

import org.springframework.security.oauth2.jwt.Jwt
import ru.itmo.ailab.backend.server.features.auth.model.dto.Role
import ru.itmo.ailab.backend.server.features.auth.model.dto.UserDto

object JwtUtil {
    fun extractRoles(jwt: Jwt): List<Role> {
        val realmAccess = jwt.getClaim<Map<String, List<String>>>("realm_access")
        return realmAccess?.get("roles")?.mapNotNull(Role::getByRoleName) ?: listOf()
    }

    fun extractUser(jwt: Jwt) = with(jwt) {
        UserDto(
            getClaimAsString("sub"),
            getClaimAsString("email"),
            getClaimAsString("preferred_username"),
            getClaimAsString("given_name"),
            getClaimAsString("middle_name"),
            getClaimAsString("family_name"),
            extractRoles(jwt)
        )
    }
}
