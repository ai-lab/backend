package ru.itmo.ailab.backend.server.features.objectimage.model.converter

import org.springframework.stereotype.Component
import ru.itmo.ailab.backend.server.features.objectimage.model.dto.ObjectImageDto
import ru.itmo.ailab.backend.server.features.objectimage.model.entity.ObjectImage
import ru.itmo.ailab.backend.server.shared.model.converter.EntityDtoConverter

@Component
class ObjectImageConverter(
    private val frameConverter: ObjectFrameConverter
) : EntityDtoConverter<ObjectImage, ObjectImageDto> {
    override fun toDto(entity: ObjectImage) = ObjectImageDto(
        entity.externalId,
        entity.objectFrame?.let { frameConverter.toDto(it) }
    )
}
