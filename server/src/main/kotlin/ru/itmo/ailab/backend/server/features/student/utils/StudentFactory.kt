package ru.itmo.ailab.backend.server.features.student.utils

import ru.itmo.ailab.backend.server.features.auth.model.dto.UserDto
import ru.itmo.ailab.backend.server.features.student.model.entity.Student

object StudentFactory {
    fun createStudent(userDto: UserDto) = Student(userDto.id, mutableListOf())
}
