package ru.itmo.ailab.backend.server.features.objectimage.model.dto

import javax.validation.Valid
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull

data class ObjectFrameDto(
    @field:Valid
    @field:NotNull
    val from: CoordinatesDto,

    @field:Valid
    @field:NotNull
    val to: CoordinatesDto,

    @field:NotBlank
    var objectClass: String,
)
