package ru.itmo.ailab.backend.server.features.objectimage.model.entity

import org.hibernate.annotations.Check
import ru.itmo.ailab.backend.server.features.objectimage.utils.ImageUtil
import javax.persistence.Column
import javax.persistence.Embeddable
import javax.validation.constraints.Max
import javax.validation.constraints.Min
import javax.validation.constraints.NotNull

@Embeddable
@Check(
    constraints = "x between 0 and ${ImageUtil.MAX_IMAGE_WIDTH} " +
            "and y between 0 and ${ImageUtil.MAX_IMAGE_HEIGHT}"
)
class Coordinates(
    @field:Min(0)
    @field:Max(ImageUtil.MAX_IMAGE_WIDTH.toLong())
    @field:NotNull
    @field:Column(nullable = false)
    var x: Double,

    @field:Min(0)
    @field:Max(ImageUtil.MAX_IMAGE_HEIGHT.toLong())
    @field:NotNull
    @field:Column(nullable = false)
    var y: Double,
)
