package ru.itmo.ailab.backend.server.features.auth.model.dto

import ru.itmo.ailab.backend.server.shared.utils.NotBlankOrNull
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull

typealias UserId = String

data class UserDto(
    @field:NotBlank
    val id: UserId,

    @field:NotBlank
    val email: String,

    @field:NotBlank
    val username: String,

    @field:NotBlank
    val firstName: String,

    @field:NotBlankOrNull
    val middleName: String?,

    @field:NotBlank
    val lastName: String,

    @field:NotNull
    val roles: List<Role>
)
