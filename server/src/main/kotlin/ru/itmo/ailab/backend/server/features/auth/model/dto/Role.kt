package ru.itmo.ailab.backend.server.features.auth.model.dto

const val ROLE_ADMIN = "ADMIN"
const val ROLE_TEACHER = "TEACHER"
const val ROLE_STUDENT = "STUDENT"

enum class Role(private val roleName: String) {
    ADMIN(ROLE_ADMIN),
    TEACHER(ROLE_TEACHER),
    STUDENT(ROLE_STUDENT);

    companion object {
        fun getByRoleName(roleName: String) = when (roleName) {
            ROLE_ADMIN -> ADMIN
            ROLE_TEACHER -> TEACHER
            ROLE_STUDENT -> STUDENT
            else -> null
        }
    }

    override fun toString() = roleName
}
