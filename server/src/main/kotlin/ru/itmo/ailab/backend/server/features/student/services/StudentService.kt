package ru.itmo.ailab.backend.server.features.student.services

import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import ru.itmo.ailab.backend.server.features.auth.model.dto.Role
import ru.itmo.ailab.backend.server.features.auth.model.dto.UserDto
import ru.itmo.ailab.backend.server.features.auth.services.AuthService
import ru.itmo.ailab.backend.server.features.student.exceptions.StudentNotFoundException
import ru.itmo.ailab.backend.server.features.student.model.entity.Student
import ru.itmo.ailab.backend.server.features.student.model.repository.StudentRepository
import ru.itmo.ailab.backend.server.features.student.utils.StudentFactory

@Service
class StudentService(
    private val studentRepository: StudentRepository,
    private val authService: AuthService,
) {
    @Transactional
    fun createAndSaveStudent(userDto: UserDto) =
        studentRepository.save(StudentFactory.createStudent(userDto))

    @Transactional
    fun currentStudent(): Student {
        val currentUser = authService.currentUser()
        if (!currentUser.roles.contains(Role.STUDENT)) throw StudentNotFoundException()
        return studentRepository.findByUserId(currentUser.id).orElseGet {
            createAndSaveStudent(currentUser)
        }
    }
}
