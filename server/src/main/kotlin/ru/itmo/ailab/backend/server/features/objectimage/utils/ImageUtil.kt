package ru.itmo.ailab.backend.server.features.objectimage.utils

object ImageUtil {
    const val MAX_IMAGE_WIDTH = 2048
    const val MAX_IMAGE_HEIGHT = 2048
}
