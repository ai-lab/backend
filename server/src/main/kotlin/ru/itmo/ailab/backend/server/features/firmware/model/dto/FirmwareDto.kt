package ru.itmo.ailab.backend.server.features.firmware.model.dto

import ru.itmo.ailab.backend.server.shared.model.entity.ExternalId
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull

data class FirmwareDto(
    @field:NotNull
    var id: ExternalId,

    @field:NotBlank
    var name: String,
)
