package ru.itmo.ailab.backend.server.features.objectimage.controller

import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.data.domain.Pageable
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*
import ru.itmo.ailab.backend.server.features.document.model.dto.FileDto
import ru.itmo.ailab.backend.server.features.objectimage.model.dto.ObjectFrameDto
import ru.itmo.ailab.backend.server.features.objectimage.services.ObjectImageService
import ru.itmo.ailab.backend.server.shared.model.entity.ExternalId
import javax.validation.Valid

@Tag(name = "Object images")
@RestController
@RequestMapping("/api/v1/object-images")
class ObjectImageController(
    private val objectImageService: ObjectImageService,
) {
    @GetMapping
    fun getObjectImageIds(pageable: Pageable) =
        objectImageService.getCurrentStudentImageIds(pageable)

    @GetMapping("/{id}")
    fun getObjectImage(@PathVariable id: ExternalId) =
        objectImageService.getObjectImage(id)

    @GetMapping("/{id}/download")
    fun downloadObjectImage(@PathVariable id: ExternalId) =
        objectImageService.downloadObjectImage(id)

    @GetMapping("/{id}/preview")
    fun previewObjectImage(@PathVariable id: ExternalId) =
        objectImageService.previewObjectImage(id)

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(consumes = [MediaType.MULTIPART_FORM_DATA_VALUE])
    fun addObjectImage(@RequestPart("image") image: FileDto) =
        objectImageService.addObjectImage(image)

    @PatchMapping("/{id}")
    fun updateObjectFrame(
        @PathVariable id: ExternalId,
        @RequestBody @Valid objectFrameDto: ObjectFrameDto
    ) = objectImageService.updateObjectFrame(id, objectFrameDto)

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/{id}")
    fun deleteImage(@PathVariable id: ExternalId) =
        objectImageService.deleteImage(id)
}
