package ru.itmo.ailab.backend.server.shared.model.dto

class PageDto<T>(
    val page: Int,
    val size: Int,
    val total: Long,
    val data: List<T>
)
