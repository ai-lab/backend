package ru.itmo.ailab.backend.server.shared.model.converter

import org.springframework.data.domain.Page
import org.springframework.stereotype.Component
import ru.itmo.ailab.backend.server.shared.model.dto.PageDto

@Component
class PageConverter {
    fun <T> toDto(entity: Page<T>) = PageDto<T>(
        entity.number + 1,
        entity.size,
        entity.totalElements,
        entity.content
    )
}
