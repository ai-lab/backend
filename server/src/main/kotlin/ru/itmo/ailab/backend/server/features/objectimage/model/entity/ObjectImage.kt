package ru.itmo.ailab.backend.server.features.objectimage.model.entity

import ru.itmo.ailab.backend.server.features.document.model.entity.Document
import ru.itmo.ailab.backend.server.features.student.model.entity.Student
import ru.itmo.ailab.backend.server.shared.model.entity.ExternalEntity
import javax.persistence.CascadeType
import javax.persistence.Entity
import javax.persistence.ManyToOne
import javax.persistence.OneToOne
import javax.validation.Valid
import javax.validation.constraints.NotNull

@Entity
class ObjectImage(
    @field:Valid
    @field:NotNull
    @field:OneToOne(optional = false)
    var image: Document,

    @field:Valid
    @field:NotNull
    @field:ManyToOne
    var student: Student,

    @field:Valid
    @field:OneToOne(mappedBy = "objectImage", cascade = [CascadeType.ALL])
    var objectFrame: ObjectFrame? = null,
) : ExternalEntity()
