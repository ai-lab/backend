package ru.itmo.ailab.backend.server.features.objectimage.model.entity

import org.hibernate.annotations.Check
import ru.itmo.ailab.backend.server.shared.model.entity.BaseEntity
import javax.persistence.AttributeOverride
import javax.persistence.AttributeOverrides
import javax.persistence.Column
import javax.persistence.Embedded
import javax.persistence.Entity
import javax.persistence.OneToOne
import javax.validation.Valid
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull

@Entity
@Check(constraints = "trim(object_class) <> ''")
class ObjectFrame(
    @field:Valid
    @field:NotNull
    @field:AttributeOverrides(
        value = [
            AttributeOverride(name = "x", column = Column(name = "from_x")),
            AttributeOverride(name = "y", column = Column(name = "from_y"))
        ]
    )
    @field:Embedded
    var from: Coordinates,

    @field:Valid
    @field:NotNull
    @field:AttributeOverrides(
        value = [
            AttributeOverride(name = "x", column = Column(name = "to_x")),
            AttributeOverride(name = "y", column = Column(name = "to_y"))
        ]
    )
    @field:Embedded
    var to: Coordinates,

    @field:NotBlank
    @field:Column(nullable = false)
    var objectClass: String,

    @field:Valid
    @field:NotNull
    @field:OneToOne(optional = false)
    var objectImage: ObjectImage
) : BaseEntity()
