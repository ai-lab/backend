package ru.itmo.ailab.backend.server.features.objectimage.model.converter

import org.springframework.stereotype.Component
import ru.itmo.ailab.backend.server.features.objectimage.model.dto.ObjectFrameDto
import ru.itmo.ailab.backend.server.features.objectimage.model.entity.ObjectFrame
import ru.itmo.ailab.backend.server.features.objectimage.model.entity.ObjectImage
import ru.itmo.ailab.backend.server.shared.model.converter.EntityDtoConverter

@Component
class ObjectFrameConverter(
    private val coordinatesConverter: CoordinatesConverter
) : EntityDtoConverter<ObjectFrame, ObjectFrameDto> {
    override fun toDto(entity: ObjectFrame) = ObjectFrameDto(
        coordinatesConverter.toDto(entity.from),
        coordinatesConverter.toDto(entity.to),
        entity.objectClass
    )

    fun toEntityWithImage(dto: ObjectFrameDto, image: ObjectImage): ObjectFrame {
        val objectFrame = ObjectFrame(
            coordinatesConverter.toEntity(dto.from),
            coordinatesConverter.toEntity(dto.to),
            dto.objectClass,
            image
        )

        val prevObjectFrame = image.objectFrame
        if (prevObjectFrame != null) {
            objectFrame.id = prevObjectFrame.id
        }

        return objectFrame
    }
}
