package ru.itmo.ailab.backend.server.features.objectimage.model.converter

import org.springframework.stereotype.Component
import ru.itmo.ailab.backend.server.features.objectimage.model.dto.CoordinatesDto
import ru.itmo.ailab.backend.server.features.objectimage.model.entity.Coordinates
import ru.itmo.ailab.backend.server.shared.model.converter.EntityDtoConverter

@Component
class CoordinatesConverter : EntityDtoConverter<Coordinates, CoordinatesDto> {
    override fun toDto(entity: Coordinates) = CoordinatesDto(
        entity.x,
        entity.y,
    )

    override fun toEntity(dto: CoordinatesDto) = Coordinates(
        dto.x,
        dto.y
    )
}
