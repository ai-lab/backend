package ru.itmo.ailab.backend.server.features.firmware.exceptions

import org.springframework.http.HttpStatus
import org.springframework.web.server.ResponseStatusException

class FirmwareNotFoundException(message: String = "Firmware not found") :
    ResponseStatusException(HttpStatus.NOT_FOUND, message)
