package ru.itmo.ailab.backend.server.features.student.exceptions

import org.springframework.http.HttpStatus
import org.springframework.web.server.ResponseStatusException

class StudentNotFoundException(message: String = "Student not found") :
    ResponseStatusException(HttpStatus.NOT_FOUND, message)
