package ru.itmo.ailab.backend.server.shared.utils

import javax.validation.Constraint
import javax.validation.ConstraintValidator
import javax.validation.ConstraintValidatorContext
import javax.validation.Payload
import kotlin.reflect.KClass

class NullOrNotBlankValidator : ConstraintValidator<NotBlankOrNull, String> {
    override fun isValid(value: String?, context: ConstraintValidatorContext) =
        value == null || value.isNotBlank()
}

@Target(AnnotationTarget.FIELD)
@MustBeDocumented
@Constraint(validatedBy = [NullOrNotBlankValidator::class])
annotation class NotBlankOrNull(
    val message: String = "{javax.validation.constraints.NotBlank.message}",
    val groups: Array<KClass<*>> = [],
    val payload: Array<KClass<out Payload>> = []
)
