package ru.itmo.ailab.backend.server.features.document.services

import io.minio.BucketExistsArgs
import io.minio.GetObjectArgs
import io.minio.GetObjectResponse
import io.minio.MakeBucketArgs
import io.minio.MinioClient
import io.minio.ObjectWriteResponse
import io.minio.PutObjectArgs
import io.minio.RemoveObjectArgs
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import ru.itmo.ailab.backend.server.features.document.exceptions.FileDeleteException
import ru.itmo.ailab.backend.server.features.document.exceptions.FileDownloadException
import ru.itmo.ailab.backend.server.features.document.exceptions.FileUploadException
import ru.itmo.ailab.backend.server.features.document.model.dto.FileDto
import javax.annotation.PostConstruct

@Service
class MinioService(
    private val minioClient: MinioClient,

    @Value("\${minio.bucket}")
    private val bucket: String
) {
    @PostConstruct
    private fun init() {
        if (!minioClient.bucketExists(BucketExistsArgs.builder().bucket(bucket).build())) {
            minioClient.makeBucket(MakeBucketArgs.builder().bucket(bucket).build())
        }
    }

    fun upload(file: FileDto, path: String): ObjectWriteResponse = runCatching {
        minioClient.putObject(
            PutObjectArgs.builder()
                .bucket(bucket)
                .`object`(path)
                .contentType(file.contentType)
                .stream(file.inputStream, file.size, -1)
                .build()
        )
    }.getOrElse { throw FileUploadException(it.message) }

    fun download(path: String): GetObjectResponse = runCatching {
        minioClient.getObject(
            GetObjectArgs.builder()
                .bucket(bucket)
                .`object`(path)
                .build()
        )
    }.getOrElse { throw FileDownloadException(it.message) }

    fun delete(path: String) = runCatching {
        minioClient.removeObject(
            RemoveObjectArgs.builder()
                .bucket(bucket)
                .`object`(path)
                .build()
        )
    }.getOrElse { throw FileDeleteException(it.message) }
}
