package ru.itmo.ailab.backend.server.features.objectimage.model.repository

import org.springframework.data.jpa.repository.JpaRepository
import ru.itmo.ailab.backend.server.features.objectimage.model.entity.ObjectFrame

interface ObjectFrameRepository : JpaRepository<ObjectFrame, Long>
