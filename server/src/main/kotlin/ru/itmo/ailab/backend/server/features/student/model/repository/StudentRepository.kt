package ru.itmo.ailab.backend.server.features.student.model.repository

import org.springframework.data.jpa.repository.JpaRepository
import ru.itmo.ailab.backend.server.features.auth.model.dto.UserId
import ru.itmo.ailab.backend.server.features.student.model.entity.Student
import java.util.Optional

interface StudentRepository : JpaRepository<Student, Long> {
    fun findByUserId(userId: UserId): Optional<Student>
}
