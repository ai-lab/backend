package ru.itmo.ailab.backend.server.features.document.model.repository

import org.springframework.data.jpa.repository.JpaRepository
import ru.itmo.ailab.backend.server.features.document.model.entity.Document
import ru.itmo.ailab.backend.server.shared.model.entity.BaseId

interface DocumentRepository : JpaRepository<Document, BaseId>
