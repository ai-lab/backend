package ru.itmo.ailab.backend.server.features.document.exceptions

import org.springframework.http.HttpStatus
import org.springframework.web.server.ResponseStatusException

const val reasonUnknown = "Reason unknown"

class FileUploadException(reason: String?) :
    ResponseStatusException(
        HttpStatus.INTERNAL_SERVER_ERROR,
        "Could not upload file: ${reason ?: reasonUnknown}"
    )

class FileDownloadException(reason: String?) :
    ResponseStatusException(
        HttpStatus.INTERNAL_SERVER_ERROR,
        "Could not download file: ${reason ?: reasonUnknown}"
    )

class FileDeleteException(reason: String?) :
    ResponseStatusException(
        HttpStatus.INTERNAL_SERVER_ERROR,
        "Could not delete file: ${reason ?: reasonUnknown}"
    )
