package ru.itmo.ailab.backend.server.features.firmware.services

import org.springframework.data.domain.Pageable
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import ru.itmo.ailab.backend.server.features.auth.model.dto.ROLE_ADMIN
import ru.itmo.ailab.backend.server.features.document.model.dto.FileDto
import ru.itmo.ailab.backend.server.features.document.services.DocumentService
import ru.itmo.ailab.backend.server.features.firmware.exceptions.FirmwareNotFoundException
import ru.itmo.ailab.backend.server.features.firmware.model.converter.FirmwareConverter
import ru.itmo.ailab.backend.server.features.firmware.model.dto.FirmwareDto
import ru.itmo.ailab.backend.server.features.firmware.model.entity.Firmware
import ru.itmo.ailab.backend.server.features.firmware.model.repository.FirmwareRepository
import ru.itmo.ailab.backend.server.shared.model.converter.PageConverter
import ru.itmo.ailab.backend.server.shared.model.entity.ExternalId

@Service
class FirmwareService(
    private val firmwareRepository: FirmwareRepository,
    private val firmwareConverter: FirmwareConverter,
    private val pageConverter: PageConverter,
    private val documentService: DocumentService,
) {
    companion object {
        private const val FIRMWARES_DIRECTORY = "firmwares"
    }

    private fun findFirmwareExternalIds(pageable: Pageable) =
        firmwareRepository.findAllExternalIds(pageable)

    private fun findFirmwareByExternalId(externalId: ExternalId) =
        firmwareRepository.findByExternalId(externalId)
            .orElseThrow { FirmwareNotFoundException() }

    fun getFirmwareIds(pageable: Pageable) =
        pageConverter.toDto(findFirmwareExternalIds(pageable))

    fun getFirmware(externalId: ExternalId) =
        firmwareConverter.toDto(findFirmwareByExternalId(externalId))

    fun downloadFirmware(externalId: ExternalId) =
        documentService.download(findFirmwareByExternalId(externalId).file)

    @PreAuthorize("hasRole('$ROLE_ADMIN')")
    @Transactional
    fun addFirmware(firmware: FileDto, name: String?): FirmwareDto {
        val document = documentService.upload(firmware, FIRMWARES_DIRECTORY)
        return firmwareConverter.toDto(firmwareRepository.save(Firmware(name ?: document.filename, document)))
    }

    @PreAuthorize("hasRole('$ROLE_ADMIN')")
    @Transactional
    fun deleteFirmware(externalId: ExternalId) {
        val firmware = findFirmwareByExternalId(externalId)
        firmwareRepository.delete(firmware)
        documentService.delete(firmware.file)
    }
}
