version: "3.9"

services:
  keycloak-postgres:
    container_name: ${KEYCLOAK_POSTGRES_NAME}
    restart: always
    image: postgres:15.2-alpine
    healthcheck:
      test:
        [
          "CMD-SHELL",
          "pg_isready -U ${KEYCLOAK_POSTGRES_USERNAME} -d ${KEYCLOAK_POSTGRES_DATABASE}"
        ]
      interval: 2s
      timeout: 2s
      retries: 60
    environment:
      POSTGRES_DATABASE: ${KEYCLOAK_POSTGRES_DATABASE}
      POSTGRES_USER: ${KEYCLOAK_POSTGRES_USERNAME}
      POSTGRES_PASSWORD: ${KEYCLOAK_POSTGRES_PASSWORD}
    ports:
      - "${KEYCLOAK_POSTGRES_PORT}:5432"
    volumes:
      - ${KEYCLOAK_POSTGRES_HOST_PATH}:/var/lib/postgresql/data

  keycloak:
    container_name: ${KEYCLOAK_NAME}
    restart: always
    image: quay.io/keycloak/keycloak:21.0
    command: start-dev --import-realm
    depends_on:
      keycloak-postgres:
        condition: service_healthy
    healthcheck:
      test: "curl --fail --silent http://${KEYCLOAK_HOST}:${KEYCLOAK_HTTP_PORT}/health | grep UP || exit 1"
      interval: 2s
      timeout: 2s
      retries: 60
    environment:
      KEYCLOAK_ADMIN: ${KEYCLOAK_ADMIN_USERNAME}
      KEYCLOAK_ADMIN_PASSWORD: ${KEYCLOAK_ADMIN_PASSWORD}
      KC_DB: postgres
      KC_DB_URL: ${KEYCLOAK_POSTGRES_URL}
      KC_DB_USERNAME: ${KEYCLOAK_POSTGRES_USERNAME}
      KC_DB_PASSWORD: ${KEYCLOAK_POSTGRES_PASSWORD}
      KC_HOSTNAME: ${KEYCLOAK_HOST}
      KC_HTTPS_KEY_STORE_FILE: ${SSL_P12_PATH}
      KC_HTTPS_KEY_STORE_PASSWORD: ${SSL_P12_PASSWORD}
      KC_HEALTH_ENABLED: true
    ports:
      - "${KEYCLOAK_HTTPS_PORT}:8443"
      - "${KEYCLOAK_HTTP_PORT}:8080"
    volumes:
      - ${SSL_HOST_PATH}:${SSL_CONTAINER_PATH}
      - ${KEYCLOAK_PROVIDERS_HOST_PATH}:/opt/keycloak/providers
      - ${KEYCLOAK_REALMS_HOST_PATH}:/opt/keycloak/data/import

  minio:
    container_name: ${MINIO_NAME}
    restart: always
    image: quay.io/minio/minio:RELEASE.2023-02-22T18-23-45Z.fips
    command: server /data --console-address ":9001"
    ports:
      - "${MINIO_CLIENT_PORT}:9000"
      - "${MINIO_CONSOLE_PORT}:9001"
    healthcheck:
      test: "curl --fail --silent http://localhost:${MINIO_CLIENT_PORT}/minio/health/live"
      interval: 2s
      timeout: 2s
      retries: 60
    environment:
      MINIO_ROOT_USER: ${MINIO_USERNAME}
      MINIO_ROOT_PASSWORD: ${MINIO_PASSWORD}
      MINIO_ACCESS_KEY: ${MINIO_ACCESS_KEY}
      MINIO_SECRET_KEY: ${MINIO_SECRET_KEY}
    volumes:
      - ${MINIO_HOST_PATH}:/data

  server-postgres:
    container_name: ${SERVER_POSTGRES_NAME}
    restart: always
    image: postgres:15.2-alpine
    healthcheck:
      test:
        [
          "CMD-SHELL",
          "pg_isready -U ${SERVER_POSTGRES_USERNAME} -d ${SERVER_POSTGRES_DATABASE}"
        ]
      interval: 2s
      timeout: 2s
      retries: 60
    environment:
      POSTGRES_DATABASE: ${SERVER_POSTGRES_DATABASE}
      POSTGRES_USER: ${SERVER_POSTGRES_USERNAME}
      POSTGRES_PASSWORD: ${SERVER_POSTGRES_PASSWORD}
    ports:
      - "${SERVER_POSTGRES_PORT}:5432"
    volumes:
      - ${SERVER_POSTGRES_HOST_PATH}:/var/lib/postgresql/data
